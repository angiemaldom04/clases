//alert('Hola');

//if if/else

/*let primerNombre = 'Angie';

if(primerNombre){
    alert('Primer nombre si existe');
}else{
    alert('Primer nombre no existe');
}*/

//if if/else

/*
let primerNombre = 'Angie';

if(primerNombre === 'Angie'){
    alert('Primer nombre correcto');
}else{
    alert('nombre incorrecto');
}
*/

// SWITCH

/*
let diaDeLaSemana = 'Martes';

switch(diaDeLaSemana){
    case 'lunes':
        alert('primer día de la semana');
        break
    case 'martes':
        alert('segundo día de la semana');
        break
    case 'miércoles':
        alert('tercer día de la semana');
        break
    case 'jueves':
        alert('cuarto día de la semana');
        break
    case 'viernes':
        alert('quinto día de la semana');
        break
    case 'sábado':
        alert('sexto día de la semana');
        break
    case 'domingo':
        alert('séptimo día de la semana');
        break
    default:
        alert('no es un día de la semana');
        break
}
*/

//Otro ejemplo (siempre se usa comilla doble "")

/*
let diaDeLaSemana;

diaDeLaSemana = window.prompt("escriba un día de la semana");

switch(diaDeLaSemana){
    case 'lunes':
        alert('primer día de la semana');
        break
    case 'martes':
        alert('segundo día de la semana');
        break
    case 'miércoles':
        alert('tercer día de la semana');
        break
    case 'jueves':
        alert('cuarto día de la semana');
        break
    case 'viernes':
        alert('quinto día de la semana');
        break
    case 'sábado':
        alert('sexto día de la semana');
        break
    case 'domingo':
        alert('séptimo día de la semana');
        break
    default:
        alert('no es un día de la semana');
        break
}
*/

//

// let a = 35;

// let b = 20;

// if (a > b){
//     alert ("a es mayor que b")
// }

//Ejercicio edad

/*
let edad;

edad = window.prompt("escriba su edad");

let a = 18;

if (a > edad){
    alert ("Eres menor de edad");
}

else if(a == edad){
    alert("Felicitaciones tienes 18");
}

else{
    alert("Eres mayor de edad");
}
*/

let nombre1 = "Juan";

let nombre2 = "Juan";

/*if (nombre1 === nombre1){
    alert("Los valores son iguales");
}else{
    alert("Los valores son distintos");
}*/

/*
console.log("el tipo de variable en numero1 es:",typeof(nombre1));
console.log("el tipo de variable en numero2 es:",typeof(nombre2));
*/

//Ejercicio
//Defino las variables que utilizaré en el ejercicio
let numero1, numero2, suma, entero1, entero2;

//Capturo lo que ingresa el usuario y lo guardo en las variables numero1 y numero2
numero1 = window.prompt("Escriba un número");
numero2 = window.prompt("Escriba otro número");

//Convierto a números enteros lo que viene en las dos variables y lo almaceno en la variable entero1 y entero2 
entero1 = parseInt(numero1);
entero2 = parseInt(numero2);

//Realizando la suma y guardándola en la variable con ese nombre
suma = entero1 + entero2;
resta = entero1 - entero2;
multiplicacion = entero1 * entero2;
division = entero1 / entero2;
residuo = entero1 % entero2;

//Escribe en pantalla
document.writeln("Este es un ejercicio");
document.writeln("<h1>La suma entre "+ numero1+" y "+numero2+" es:"+suma+"</h1>");
document.writeln("<h1>La resta entre "+ numero1+" y "+numero2+" es:"+resta+"</h1>");
document.writeln("<h1>La multiplicación entre "+ numero1+" y "+numero2+" es:"+multiplicacion+"</h1>");
document.writeln("<h1>La división entre "+ numero1+" y "+numero2+" es:"+division+"</h1>");
document.writeln("<h1>La multiplicación entre "+ numero1+" y "+numero2+" es:"+residuo+"</h1>");